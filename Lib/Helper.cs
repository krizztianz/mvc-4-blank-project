﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCrypt.Net;

namespace MVC4FW.Lib
{
    public class Helper
    {
        //Genarate SHA from string
        public static string GenerateSHA(string value)
        {
            try
            {
                var hash = System.Security.Cryptography.SHA1.Create();
                var encoder = new System.Text.ASCIIEncoding();
                var combined = encoder.GetBytes(value ?? "");
                return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
            }
            catch (Exception)
            {
                return null;
            }
        }

        //Genarate SHA from string
        public static string GenerateMD5(string value)
        {
            try
            {
                var hash = System.Security.Cryptography.MD5.Create();
                var encoder = new System.Text.ASCIIEncoding();
                var combined = encoder.GetBytes(value ?? "");
                return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static String generateUUID()
        {
            return System.Guid.NewGuid().ToString();
        }

        public static string ToTitleCase(string text)
        {
            try
            {
                return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(text.ToLower());
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}